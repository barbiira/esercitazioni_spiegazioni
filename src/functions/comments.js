import { Comment } from "../models/comment";

/**
 * Create a comment accordingly to the given params
 *
 * @param {string} email User's email
 * @param {string} name User's name
 * @param {string} body Content of the comment
 * @param {number} postId Id of the post
 * @returns {Promise<Comment | null>}
 */
export const addComment = async (email, name, body, postId) => {
  const response = await fetch(
    `http://jsonplaceholder.typicode.com/posts/${postId}/comments`,
    {
      body: JSON.stringify({
        // È una contrazione per body: body, email: email etc etc
        body,
        email,
        name,
        postId,
      }),
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      method: "POST",
    }
  );

  const data = await response.json();

  // Analizzo il codice della risposta e restituisco un valore di conseguenza
  return response.status === 201
    ? new Comment(data.id, data.email, data.name, data.body, data.postId)
    : null;
};

/**
 * Deletes a comment for the given id
 *
 * @param {number} id The id of the Comment to delete
 * @returns {Promise<Comment | null>}
 */
export const deleteComment = async id => {
  const response = await fetch(
    `http://jsonplaceholder.typicode.com/comments/${id}`,
    {
      method: "DELETE",
    }
  );

  const data = response.json();

  // Analizzo il codice della risposta e restituisco un valore di conseguenza
  return response.status === 200
    ? new Comment(data.id, data.email, data.name, data.body, data.postId)
    : null;
};

export const getComment = async id => {
  const response = await fetch(
    `http://jsonplaceholder.typicode.com/comments/${id}`
  );
  const data = await response.json();

  // Analizzo il codice della risposta e restituisco un valore di conseguenza
  return response.status === 200
    ? new Comment(data.id, data.email, data.name, data.body, data.postId)
    : null;
};

/**
 * Fetch the comments for the given post id
 *
 * @param {number} postId The post Id
 * @returns {Promise<Comment[]>}
 */
export const getComments = async postId => {
  const response = await fetch(
    `http://jsonplaceholder.typicode.com/posts/${postId}/comments`
  );
  const data = await response.json();

  return data.map(c => new Comment(c.id, c.email, c.name, c.body, c.postId));
};

export const putComment = async (id, email, name, body, postId) => {
  const response = await fetch(
    `http://jsonplaceholder.typicode.com/comments/${id}`,
    {
      // Il verbo PUT trasferisce informazioni come il verbo POST, ovvero attraverso il body della richiesta
      // Ricorda di trasformarlo in una stringa rispettosa del formato JSON
      body: JSON.stringify({
        body,
        email,
        id,
        name,
        postId,
      }),
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      // Il verbo PUT serve a modificare dati
      // A differenza di PATCH, impone che nella richiesta sia presente l'entità nella sua interezza
      method: "PUT",
    }
  );

  const data = await response.json();

  return new Comment(data.id, data.email, data.name, data.body, data.postId);
};

// // Per importare "putComment" -> import { putComment } from '...';

// // L'export per default consiste nell'esportazione di un elemento senza nome
// // Può esserci un solo elemento esportato per default per file
// export default {
//   name: ''
// };

// // Un elemento esportato per default viene importato senza le graffe
// // import nomeDaDareAllElemento from '...'
