import Chart from "chart.js/auto";

// Prelevo il contesto del canvas nel quale poter disegnare
const ctx = document.getElementById("chart").getContext("2d");

// Chart data
const data = [];
const labels = [];

const chart = new Chart(ctx, {
  type: "line",
  data: {
    labels,
    datasets: [
      {
        label: "My First Dataset",
        data,
        fill: false,
        borderColor: "rgb(75, 192, 192)",
        tension: 0.1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
});

let lastYear = 1960;

setInterval(() => {
  data.push(+(Math.random() * 100).toFixed());
  //++, anno incrementato
  labels.push(++lastYear);
  chart.update();
}, 1000);
