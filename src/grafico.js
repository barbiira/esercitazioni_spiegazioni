import Chart from "chart.js/auto";

//const bum = [20, 20, 10, 50, 20, 20];

// A casa
//const start = Date.now();

//  setInterval(() => {
//    const millis = Date.now() - start;
//    for (let i = 0; i < bum.length; i++) {
//      data.push(bum[i]);
//      labels.push(Math.floor(millis / 1000));
//    }
//    chart.update();
//  }, 1000);

//CORREZIONE
// Prelevo il contesto del canvas nel quale poter disegnare
const ctx = document.getElementById("chart").getContext("2d");

// Chart data
let chart = initChart();
let data = [];
let i = 1;
let labels = [];

function initChart() {
  return new Chart(ctx, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          label: "My First Dataset",
          data,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ],
    },
    options: {
      animation: false,
      scales: {
        x: {
          beginAtZero: true,
          max: 140,
        },
        y: {
          beginAtZero: true,
          max: 100,
        },
      },
    },
  });
}

setInterval(() => {
  // Parte iniziale e finale del grafico
  if (i <= 20 || i >= 120) {
    data.push(50);
  } else if (i > 45 && i <= 95) {
    // else if: discesa
    data.push(data[data.length - 1] - 1);
  } else {
    // salita
    data.push(data[data.length - 1] + 1);
  }

  labels.push(i);
  chart.update();

  if (i === 140) {
    console.log(data);
    i = 0;
    data = [];
    labels = [];
    chart.destroy();
    chart = initChart();
  }
  i++;
}, 100);
