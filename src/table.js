function User(id, email, name) {
  this.email = email;
  this.id = id;
  this.name = name;
}

var users = [
  new User("a1", "uno@primo.com", "One"),
  new User("a2", "due@secondo.com", "Two"),
  new User("a3", "tre@terzo.com", "Three"),
  new User("a4", "quattro@quarto.com", "Four"),
  new User("a5", "cinque@quinto.com", "Five"),
];
console.log(users);
for (var i = 0; i < users.length; i++) {
  corpo.innerHTML +=
    '<tr id="' +
    users[i] +
    '"><td>' +
    users[i].id +
    "</td><td>" +
    users[i].email +
    "</td><td>" +
    users[i].name +
    "</td><td>" +
    "<button class = delete>Cancella</button>" +
    "</td></tr>";
}

var buttons = document.querySelectorAll("button.delete");

// buttons.forEach(function(b) {
//     b.addEventListener('click', function(e) {
//         e.target.parentElement.parentElement.remove();
//     });
// })

buttons.forEach(function (b) {
  b.addEventListener("click", function (e) {
    var r = e.target.parentElement.parentElement;
    // e.target.remove(users);
    //users.splice(index, 1);
    // Su ciascuna riga, abbiamo inserito nell'attributo "id"
    // il valore "id" dell'utente
    // Questo serve a poter ricercare l'utente da elminare all'interno dell'array
    // Itero su ciascun elemento dell'array fintanto che non individuo l'utente che ha
    // un id uguale alla variabile userId
    var userId = r.id;

    r.remove();

    var pos = users.findIndex(function (u) {
      return u.id == userId;
    });

    // Parametro 1: elimina a partire dalla posizione indicata
    // Parametro 2: numero di elementi da eliminare
    users.splice(pos, 1);
    console.log(users);
  });
});
