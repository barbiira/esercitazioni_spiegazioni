module.exports = {
    devtool: 'source-map',
    entry: {
        socket: './src/socket.js',
        chart: './src/chart.js',
        comment: './src/comment.js',
        grafico: './src/grafico.js',
        main: './src/app.js',
        meteo: './src/meteo.js',
        post: './src/post.js',
        table: './src/table.js',
        users: './src/users.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}